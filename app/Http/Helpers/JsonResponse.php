<?php
namespace App\Http\Helpers;

class JsonResponse {
    public static function success($data = [], $message = '', $code = 200) {
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message,
        ], $code);
    }

    public static function error($message = '', $code = 400) {
        return response()->json([
            'success' => false,
            'message' => $message,
        ], $code);
    }
}
