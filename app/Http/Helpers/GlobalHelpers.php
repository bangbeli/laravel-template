<?php
namespace App\Http\Helpers;

class GlobalHelpers {
    public static function formatRupiah($value) {
        return 'Rp. '.number_format($value, 0, ',', '.');
    }
}
